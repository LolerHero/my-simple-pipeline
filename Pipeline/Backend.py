import datetime
import unittest
from unittest import mock

Savestate = [['21.09.2019', 'Hallo']]


def menu():
    print("1. New Entry")
    print("2. Edit Entry")
    print("3. Delete Entry")
    print("4. View all Entries")
    print("9. Exit")
    print("")
    selection = input("Please select one of the above options: ")

    if selection == "1":
        while True:
            inputdate = input("Please enter a valid Date (dd.mm.yyyy): ")
            is_valid_input = validate_input(inputdate)

            if is_valid_input:
                is_valid_date = validate_date(inputdate)
                if is_valid_date:
                    break
        while True:
            inputtext = input("Please enter your Text: ")

            inputtext = inputtext.strip()

            is_valid_text = validate_input(inputtext)

            if len(inputtext) <= 0:
                print("Text must be > than Spaces!")
            if is_valid_text:
                break
        new_entry_succesfull = new_entry(inputdate, inputtext)

        if not new_entry_succesfull:
            print("Your entry has not been added!")

        else:
            print("Your entry has been added")
    if selection == "2":
        succeded = edit_entry()

        if succeded:
            print("Your entry has been updated")
        else:
            print("Something went wrong please try again!")
    if selection == "3":
        succeded = delete_entry()

        if succeded:
            print("The selected entry has been removed!")
        else:
            print("Something went wrong please try again!")
    if selection == "4":
        show_all_entries()
    if selection == "9":
        exit()
    else:
        print("Your selection was invalid, please try again!")


def validate_input(date):
    is_valid = True

    if len(date) >= 0:
        if len(date) < 3:
            print("The Input is not valid! Please try again.")
            is_valid = False
            return is_valid
    return is_valid


def validate_date(date):
    is_valid = True

    try:
        day, month, year = date.split('.')
    except ValueError:
        is_valid = False

    if is_valid:
        try:
            datetime.datetime(int(year), int(month), int(day))
        except ValueError:
            is_valid = False

    return is_valid


def new_entry(date, text):
    localarray = [date, text]

    exists = check_for_entry(date)

    if exists:
        for i in range(len(Savestate)):
            if date == Savestate[i][0]:
                Savestate.pop(i)
        Savestate.append(localarray)
        return True
    else:
        Savestate.append(localarray)
        return True


def check_for_entry(date):
    existing_flag = False

    if len(Savestate) > 0:
        i = 0
        while i < len(Savestate):
            if Savestate[i][0] != date:
                i += 1
            else:
                print("There is already an existing Entry!")
                existing_flag = True
                break
    return existing_flag


def show_all_entries():
    for x in range(len(Savestate)):
        for i in range(len(Savestate[x])):
            print(Savestate[x][i], end=' ')
        print()
    print()


def edit_entry():
    show_all_entries()
    dateSelection = input("Please Select the Date of the Entry you want to edit (dd.mm.yyyy): ")

    valid = validate_input(dateSelection)

    if not valid:
        return False

    valid = validate_date(dateSelection)

    if not valid:
        return False
    else:
        try:
            for i in range(len(Savestate)):
                Savestate[i].index(dateSelection)
        except ValueError:
            return False

        for i in range(len(Savestate)):
            if Savestate[i][0] == dateSelection:
                string = input("Please enter your text: ")
                if len(string.strip()) == 0:
                    return False
                else:
                    Savestate[i][1] = string.strip()
                    return True
            else:
                return False


def delete_entry():
    show_all_entries()
    entry = input("Please select the date of the Entry you want to delete: ")

    valid = validate_input(entry)

    if not valid:
        return False

    valid = validate_date(entry)
    if valid:
        succeded = True

        try:
            for i in range(len(Savestate)):
                Savestate[i].index(entry)
        except ValueError:
            succeded = False
        try:
            for i in range(len(Savestate)):
                if entry == Savestate[i][0]:
                    Savestate.pop(i)
        except IndexError:
            succeded = False
    return succeded


class TestBeispieleTestCase(unittest.TestCase):
    def test_check_for_entry(self):
        self.assertEqual(True, check_for_entry('21.09.2019'))

    def test_validate_date(self):
        self.assertEqual(True, validate_date('21.09.2019'))

    def test_new_entry(self):
        self.assertEqual(True, new_entry('22.09.2019', 'Hallo'))


if __name__ == '__main__':
    unittest.main()
