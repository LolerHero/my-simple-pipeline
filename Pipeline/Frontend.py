import datetime
import tkinter as tk

Savestate = []
LARGE_FONT = ("Arial", 12)


class Application(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.wm_title(self, "Custom Title")
        tk.Tk.geometry(self, "750x620")

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        label = tk.Label(self, text="Start Page", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = tk.Button(self, text="New Entry",
                             command="")
        button1.pack()

        Button2 = tk.Button(self, text="Edit Entry",
                             command="")
        button2.pack()

        Button3 = tk.Button(self, text="Delete Entry",
                             command="")
        button3.pack()

        Button4 = tk.Button(self, text="View all Entries",
                             command="")
        button4.pack()

        button5 = tk.Button(self, text="Exit",
                             command=exit)
        button5.pack()


def menu():
    print("1. New Entry")
    print("2. Edit Entry")
    print("3. Delete Entry")
    print("4. View all Entries")
    print("9. Exit")
    print("")
    selection = input("Please select one of the above options: ")

    if selection == "1":
        while True:
            inputdate = input("Please enter a valid Date (dd.mm.yyyy): ")
            is_valid_input = validate_input(inputdate)

            if is_valid_input:
                is_valid_date = validate_date(inputdate)
                if is_valid_date:
                    break
        while True:
            inputtext = input("Please enter your Text: ")

            inputtext = inputtext.strip()

            is_valid_text = validate_input(inputtext)

            if len(inputtext) <= 0:
                print("Text must be > than Spaces!")
            if is_valid_text:
                break
        new_entry_succesfull = new_entry(inputdate, inputtext)

        if not new_entry_succesfull:
            print("Your entry has not been added!")
            menu()
        else:
            print("Your entry has been added")
            menu()
    if selection == "2":
        succeded = edit_entry()

        if succeded:
            print("Your entry has been updated")
            menu()
        else:
            print("Something went wrong please try again!")
            menu()
    if selection == "3":
        succeded = delete_entry()

        if succeded:
            print("The selected entry has been removed!")
            menu()
        else:
            print("Something went wrong please try again!")
            menu()
    if selection == "4":
        show_all_entries()
        menu()
    if selection == "9":
        exit()
    else:
        print("Your selection was invalid, please try again!")
        menu()


def validate_input(date):
    is_valid = True

    if len(date) >= 0:
        if len(date) < 3:
            print("The Input is not valid! Please try again.")
            is_valid = False
            return is_valid
    return is_valid


def validate_date(date):
    is_valid = True

    try:
        day, month, year = date.split('.')
    except ValueError:
        is_valid = False

    if is_valid:
        try:
            datetime.datetime(int(year), int(month), int(day))
        except ValueError:
            is_valid = False

    return is_valid


def new_entry(date, text):
    localarray = [date, text]

    exists = check_for_entry(date)

    if exists:
        print("Do you want to overwrite the Entry?")
    else:
        print("Do you want to add this entry: " + date + ": " + text + "?")

    choice = input("Y/N: ")

    if not exists:
        if choice.upper() == "Y":
            Savestate.append(localarray)
            file = open("Log.txt", "a")
            file.write("Adding Entry Succesfull")
            file.write("/")
            file.close
            return True
        if choice.upper() == "N":
            file = open("Log.txt", "a")
            file.write("Adding Entry Unsuccesfull")
            file.write("/")
            file.close
            return False
    else:
        if choice.upper() == "Y":
            for i in range(len(Savestate)):
                if date == Savestate[i][0]:
                    Savestate.pop(i)
            Savestate.append(localarray)
            return True
        if choice.upper() == "N":
            return False



def check_for_entry(date):
    existing_flag = False

    if len(Savestate) > 0:
        i = 0
        while i < len(Savestate):
            if Savestate[i][0] != date:
                i += 1
            else:
                print("There is already an existing Entry!")
                existing_flag = True
                break
    return existing_flag


def show_all_entries():
    for x in range(len(Savestate)):
        for i in range(len(Savestate[x])):
            print(Savestate[x][i], end=' ')
        print()
    print()


def edit_entry():
    show_all_entries()
    dateSelection = input("Please Select the Date of the Entry you want to edit (dd.mm.yyyy): ")

    valid = validate_input(dateSelection)

    if not valid:
        return False

    valid = validate_date(dateSelection)

    if not valid:
        return False
    else:
        try:
            for i in range(len(Savestate)):
                Savestate[i].index(dateSelection)
        except ValueError:
            return False

        for i in range(len(Savestate)):
            if Savestate[i][0] == dateSelection:
                 string = input("Please enter your text: ")
                 if len(string.strip()) == 0:
                     return False
                 else:
                    Savestate[i][1] = string.strip()
                    return True
            else:
                return False


def delete_entry():

    show_all_entries()
    entry = input("Please select the date of the Entry you want to delete: ")

    valid = validate_input(entry)

    if not valid:
        return False

    valid = validate_date(entry)
    if valid:
        succeded = True

        try:
            for i in range(len(Savestate)):
                Savestate[i].index(entry)
        except ValueError:
            succeded = False
        try:
            for i in range(len(Savestate)):
                if entry == Savestate[i][0]:
                    Savestate.pop(i)
        except IndexError:
            succeded = False
    return succeded


app = Application()
app.mainloop()
